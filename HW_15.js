/**
 * Created by Алина on 15.01.2022.
 */

// Теоретичні питання
// Напишіть, як ви розумієте рекурсію. Навіщо вона використовується на практиці?
   // Рекурсія - це функція, що викликає сама себе

function getFactorial(n) {
    if (n === 1 || n === 0) {
        return 1
    } else {
        return n * getFactorial(n - 1)
    }
};
let result = getFactorial(6);
console.log(result)